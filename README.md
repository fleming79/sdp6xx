#SDP6XX#
Code for communicating with the Sensirion SDP6XX sensors. 

##Connection##
Connect sensor to I2C pins. Warning: SDP6XX sensors are 3.3V so a level shift is technically required. Data will be streamed out over serial.
##Settings##
* Default Baud rate: 115200
* Sample bit depth: set_resolution() [11bit default]
* Sample rate: DT [200Hz default]
* SCALEFACTOR: must be changed depending on the sensor type. The default SCALEFACTOR is for the 125Pa sensor.

Thanks to Peter N for writing most of this code.