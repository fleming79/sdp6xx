//
// Driver for DAQ via Serial
//

#include <Wire.h>
#include<stdlib.h>

// Sample Rate
const int DT = 5; // ms (1000/DT Hz)

// Scale factor see sensor documentation 
// 500pa -> 60 Pa-1
// 125pa -> 240 Pa-1
// 25pa -> 1200 Pa-1
const float SCALEFACTOR  = 240; // Pa-1

// Sensor Count
const int SENSORS = 1;


// Sensor Address
const int ADDR = 0x40;

// Sensor Commands
typedef enum {
   MEASUREMENT_PA_HM   = 0xF1, // update for pressure measurement, hold master
   MEASUREMENT_T_HM    = 0xE3, // Temperature measurement, hold master
   MEASUREMENT_RH_HM   = 0xE5, // Humidity measurement,  hold master
   MEASUREMENT_T_POLL  = 0xF3, // Temperature measurement, no hold master, currently not used
   MEASUREMENT_RH_POLL = 0xF5, // Humidity measurement, no hold master, currently not used
   USER_REG_W_ADV      = 0xE4, // write advanced user register
   USER_REG_R_ADV      = 0xE5, // read advanced user register
   USER_REG_W          = 0xE6, // write user register
   USER_REG_R          = 0xE7, // read user register
   SOFT_RESET          = 0xFE  // soft reset
} SDP6xxCommand;

typedef enum {
   RESOLUTION_9BIT = 0x00,
   RESOLUTION_10BIT = 0x01,
   RESOLUTION_11BIT = 0x02,
   RESOLUTION_12BIT = 0x03,
   RESOLUTION_13BIT = 0x04,
   RESOLUTION_14BIT = 0x05,
   RESOLUTION_15BIT = 0x06,
   RESOLUTION_16BIT = 0x07
} SDP6xxResolutions;

// State
unsigned long clock = millis();


///////////////////////////////// select //////////////////////////////////////
bool select(int index)
{

  return true;
}


///////////////////////////////// checksum /////////////////////////////////////
byte checksum(byte *data, int n)
{
  byte crc = 0;

  for(int i = 0; i < n; ++i)
  {
    crc ^= data[i];
    
    for(int j = 8; j > 0; --j)
    {
      if (crc & 0x80)
        crc = (crc << 1) ^ 0x131;
      else
        crc = (crc << 1);
    }
  }
  
  return crc;
}


///////////////////////////////// read /////////////////////////////////////////
bool read_request(uint8_t command, int16_t *value)
{
  // Request Measurement
  Wire.beginTransmission(ADDR);
  Wire.write(command);
  
  return (Wire.endTransmission() == 0);
}

///////////////////////////////// read /////////////////////////////////////////
bool read_complete(uint8_t command, int16_t *value)
{
  byte data[2];

  // Wait for Measurement
  Wire.requestFrom(ADDR, 3);

  if (Wire.available() < 3)
    return false;
  
  // Receive Measurement
  data[0] = Wire.read();
  data[1] = Wire.read(); 
 
  // Check CRC
  byte crc = Wire.read();
  if (crc != checksum(data, sizeof(data)))
    return false;

  // value
  *value = (data[0] << 8) + data[1];
  
  return true;
}

///////////////////////////////// read /////////////////////////////////////////
bool read(uint8_t command, int16_t *value)
{
  return read_request(command, value) && read_complete(command, value);
}


///////////////////////////////// write ////////////////////////////////////////
bool write(uint8_t command, int16_t value)
{
  Wire.beginTransmission(ADDR);
  Wire.write(command);
  Wire.write(value >> 8);
  Wire.write(value & 0xFF);
  Wire.endTransmission();
}


///////////////////////////////// set_resolution ///////////////////////////////
bool set_resolution(uint8_t resolution)
{
  int16_t reg;

  if (!read(USER_REG_R_ADV, &reg))
    return false;

  reg &= 0xF1FF; 
  reg |= (resolution & 0x07) << 9;
  
  if (!write(USER_REG_W_ADV, reg))
    return false;
    
  return true;
}


///////////////////////////////// reset ////////////////////////////////////////
void  reset()
{
  // Soft Reset
  Wire.beginTransmission(ADDR);
  Wire.write(SOFT_RESET);
  delay(15);  
}


///////////////////////////////// transmit /////////////////////////////////////
void transmit(char *buffer)
{
  char *ch = &buffer[1];
  
  int checksum = 0;
  for( ; *ch != '*'; ++ch)
    checksum = checksum ^ *ch;

  sprintf(++ch, "%02X", checksum);
  
  Serial.println(buffer);
}


///////////////////////////////// setup ////////////////////////////////////////
void setup()
{
  Wire.begin();
   

  for(int i = 0; i < SENSORS; ++i)
  {
    select(i);
    set_resolution(RESOLUTION_11BIT);
  }

  Serial.begin(115200);  
  Serial.println("DAQ I2C Serial Interface Begin");
}


///////////////////////////////// loop /////////////////////////////////////////
void loop()
{
  bool ok = true;
  int errsor = -1;

  int16_t values[SENSORS];
  
  for(int i = 0; i < SENSORS; ++i)
  {
    ok = ok && select(i) && read(MEASUREMENT_PA_HM, &values[i]);
    
    if (!ok && errsor < 0)
      errsor = i;
  }

  if (ok)
  {
    int n = 0;
    char buffer[1024];
    char buff[18];
    
    n += sprintf(&buffer[n], "$SAMPL,%lu", millis());

    for(int i = 0; i < SENSORS; ++i)
//       n += sprintf(&buffer[n], "%d", values[i]); // Return un-calibrated values
      n += sprintf(&buffer[n], dtostrf(values[i]/SCALEFACTOR,12,6,buff)); // Return calibrated values

    n += sprintf(&buffer[n], "*00");

    transmit(buffer);
  }
  else
  {
    char buffer[1024];
    
    sprintf(buffer, "$ERROR,Sensor[%d] Read Error*00", errsor);
    
    transmit(buffer);

    reset();   
  }

  while (clock+DT < millis())
  {
    char buffer[1024];
    
    sprintf(buffer, "$OVRUN*00");
    
    transmit(buffer);
    
    clock += DT;
  }
  
  delay(DT - min(millis() - clock, DT));
  
  clock += DT;
}

